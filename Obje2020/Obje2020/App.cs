﻿using Obje2020;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obje2020
{
    public class App : Application
    {

        public static Usuario sesion;

        public App()
        {
            sesion = new Usuario
            {
                Nombre = "Ronald",
                Apellido = "Echeverri",
                Username = "Ron",
                Correo = "Ronaldecheverrry@gmail.com",
                Edad = "28"

            };



            MainPage = new  NavigationPage (new LoginPrincipal()); //Pila de navegación 
        }

       
    }
}