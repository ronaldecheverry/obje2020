﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obje2020
{
    public class Login : ContentPage
    {
        StackLayout VistaPrincipal,StackHorizontal;
        Entry usuarioCaja;
        Entry passwordCaja;
        Button botonIniciar;
        Label Recordar,titulo;
        bool respuesta;

        CheckBox checkRecordarme;
        public Login()
        {

            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();

        }
        void CrearVistas()
        {
            VistaPrincipal = new StackLayout
            {
                BackgroundColor = Color.White,
                Padding = new Thickness(32,0,32,0),
                VerticalOptions = LayoutOptions.CenterAndExpand,
                

            };

            StackHorizontal = new StackLayout
            {

                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.Center


            };

            usuarioCaja = new Entry
            {

                Text = "",
                Placeholder = "Usuario",
                PlaceholderColor = Core.Textos
            };
            passwordCaja = new Entry
            {
                Text = "",
                Placeholder = "Password",
                PlaceholderColor = Core.Textos,
                IsPassword = true
            };

            checkRecordarme = new CheckBox { 
                
                Color = Core.Botones,
                Margin = 0
            
            };

            Recordar = new Label
            {
                Text = "¿Desea guardar sus credenciales?",
                VerticalOptions =  LayoutOptions.Center

            };

            titulo = new Label
            {
                Text = "INICIAR SESION",
                FontSize= 30,
                HorizontalTextAlignment = TextAlignment.Center,
                Margin= new Thickness(0,0,0,30)

            };

           
            botonIniciar = new Button
            {
                Text = "Iniciar Sesion",
                BackgroundColor = Core.Botones,
                TextColor = Color.White,
                CornerRadius = 20
            };

        }
        void AgregarVistas()
        {
            VistaPrincipal.Children.Add(titulo);
            VistaPrincipal.Children.Add(usuarioCaja);
            VistaPrincipal.Children.Add(passwordCaja);


            StackHorizontal.Children.Add(checkRecordarme);
            StackHorizontal.Children.Add(Recordar);
            

            VistaPrincipal.Children.Add(StackHorizontal);
            VistaPrincipal.Children.Add(botonIniciar);


            

            Content = VistaPrincipal;
        }
        void AgregarEventos() {

            checkRecordarme.CheckedChanged += checkRecordarme_CheckedChanged;
            botonIniciar.Clicked += BotonIniciar_Clicked;


        }
        private async void BotonIniciar_Clicked(object sender, EventArgs e) {

            
                if (string.IsNullOrEmpty(usuarioCaja.Text))
                {


                    await DisplayAlert("¡Aviso!", "Ingresa tu usuario", "Aceptar");

                }

                else if (string.IsNullOrEmpty(passwordCaja.Text))
                {


                    await DisplayAlert("¡Aviso!", "Ingresa tu password", "Aceptar");


                }

                else if (!string.IsNullOrEmpty(passwordCaja.Text))
                {

                    int count = 0;

                    for (int i = 0; i < (passwordCaja.Text).Length; i++)
                    {
                        count++;
                    }

                    if (count <= 6)
                    {
                        await DisplayAlert("¡Aviso!", "Tu password debe contener 8 o más caracteres", "Aceptar");


                    }

                    else
                    {

                    if (respuesta)
                    {
                        await DisplayAlert("Recuerda", "Estás guardando tu sesión", "Aceptar");

                        await Navigation.PushAsync(new PaginaPrincipal());
                    }
                    else
                    {
                        await Navigation.PushAsync(new PaginaPrincipal());
                    }
                    }
                }
                

            
            



        }
        private async void checkRecordarme_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            respuesta = await DisplayAlert("Notificacion", "¿Quieres guardar tu sesión?", "Aceptar", "Cancelar");

            


        }

        

    }



}