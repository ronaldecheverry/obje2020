﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Xamarin.Forms;

namespace Obje2020
{
    public class RecuperarCuenta : ContentPage
    {

        RelativeLayout VistaPrincipal;
        Entry Correo;
        Button botonRecuperar;
        Label tituloRecuperar;
        TapGestureRecognizer Tap_GestoAtras;

        BoxView BarraNavegacion;
        Image Iconoatras;

        public RecuperarCuenta()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();


        }

        void CrearVistas()
        {
            VistaPrincipal = new RelativeLayout();


            Tap_GestoAtras = new TapGestureRecognizer();
            BarraNavegacion = new BoxView
            {

                BackgroundColor = Core.Navegacion


            };

            Iconoatras = new Image

            {

                Source = Core.IconoAtras

            };


            tituloRecuperar = new Label
            {

                TextColor = Core.Titulos,
                Text = "Recuperar Contraseña",
                FontSize = 21,
                HorizontalTextAlignment = TextAlignment.Center
                

            };
            Correo = new Entry
            {

                Text = "",
                Placeholder = "Correo Institucional",
                PlaceholderColor = Core.Textos
                
            };


            botonRecuperar = new Button
            {
                Text = "Recuperar",
                BackgroundColor = Core.Botones,
                BorderColor = Core.Navegacion,
                BorderWidth = 2,
                TextColor = Core.Navegacion,
                CornerRadius = 20

            };

        }
        

        void AgregarVistas()
        {
            Iconoatras.GestureRecognizers.Add(Tap_GestoAtras);
            VistaPrincipal.Children.Add(BarraNavegacion,
             Constraint.RelativeToParent((p) => { return 0; }), // X
             Constraint.RelativeToParent((p) => { return 0; }), // Y
              Constraint.RelativeToParent((p) => { return p.Width; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height * 0.083; })  //Alto
             );


            VistaPrincipal.Children.Add(tituloRecuperar,
            Constraint.RelativeToParent((p) => { return 0; }), // X
            Constraint.RelativeToParent((p) => { return p.Height * 0.022; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width; })  //Alto
            );

            VistaPrincipal.Children.Add(Iconoatras,
             Constraint.RelativeToParent((p) => { return 0; }), // X
             Constraint.RelativeToParent((p) => { return 0; }), // Y
              Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height * 0.083; })  //Alto
             );

            VistaPrincipal.Children.Add(Correo,
             Constraint.RelativeToParent((p) => { return p.Width * 0.173; }), // X
             Constraint.RelativeToParent((p) => { return p.Height * 0.45; }), // Y
              Constraint.RelativeToParent((p) => { return p.Width * 0.66; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height * 0.062; })  //Alto
             );
            VistaPrincipal.Children.Add(botonRecuperar,
             Constraint.RelativeToParent((p) => { return p.Width * 0.168; }), // X
             Constraint.RelativeToParent((p) => { return p.Height * 0.55; }), // Y
              Constraint.RelativeToParent((p) => { return p.Width * 0.66; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height * 0.062; })  //Alto
             );


            Content = VistaPrincipal;
        }

        void AgregarEventos()
        {
            Tap_GestoAtras.Tapped += Tap_GestoAtras_Tapped;
            botonRecuperar.Clicked += BotonRecuperar_Clicked;

        }

        private async void Tap_GestoAtras_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();

        }

        private async void BotonRecuperar_Clicked(object sender, EventArgs e)
        {


            if (string.IsNullOrEmpty(Correo.Text))
            {


                await DisplayAlert("¡Aviso!", "Ingresa un correo valido", "Aceptar");

            }

            else if (!string.IsNullOrEmpty(Correo.Text))
            {
               
                string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

                if (Regex.IsMatch(Correo.Text, expresion))
                {
                    if (Regex.Replace(Correo.Text, expresion, String.Empty).Length == 0)
                    {
                        await Navigation.PopAsync();
                    }
                   

                }

                else
                {
                    await DisplayAlert("¡Aviso!", "Ingresa un correo valido (example@ron.com)", "Aceptar");
                }
            }
            else 
            {

                await Navigation.PopAsync();

            }
           

                
            }







        }

    }
