﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obje2020
{
    public class DetalleNotas : ContentPage
    {
        RelativeLayout VistaPrincipal;
        BoxView BarraNavegacion;
        Image Iconoatras;
        Label TituloPagina, Resultado, Notafinal,titulonota,titulonota2,titulonota3;

        Button Nota1, Nota2, Nota3;
        Estudiante estudianteglobal;
        

        TapGestureRecognizer Tap_GestoAtras;
     
        public DetalleNotas(Estudiante estudiante  )
        {
            estudianteglobal = estudiante;
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();
         



        }

       

        void CrearVistas()
        {

            VistaPrincipal = new RelativeLayout();

            Tap_GestoAtras = new TapGestureRecognizer();

          


            BarraNavegacion = new BoxView
            {

                BackgroundColor = Core.Navegacion


            };

            Iconoatras = new Image

            {

                Source = Core.IconoAtras

            };

           

            TituloPagina = new Label

            {
                Text = "Detalle Notas",
                FontSize = 21,
                TextColor = Core.Titulos,
                HorizontalTextAlignment = TextAlignment.Center


            };

            titulonota = new Label

            {
                Text = "Parcial 1",
                FontSize = 15,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center


            };
            titulonota2 = new Label

            {
                Text = "Parcial 2",
                FontSize = 15,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center


            };
            titulonota3 = new Label

            {
                Text = "Parcial 3",
                FontSize = 15,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center


            };

            Nota1 = new Button {

                Text = estudianteglobal.Nota.ToString() ,
                BackgroundColor = Core.Navegacion,
                FontAttributes = FontAttributes.Bold,
                TextColor = Core.Titulos,
                FontSize = 25,
                CornerRadius = 20



            };
            Nota2 = new Button
            {

                Text = estudianteglobal.Nota2.ToString(),
                BackgroundColor = Core.Navegacion,
                FontAttributes = FontAttributes.Bold,
                TextColor = Core.Titulos,
                FontSize = 25,
                CornerRadius = 20
                


            };
            Nota3 = new Button
            {


                Text = estudianteglobal.Nota3.ToString(),
                BackgroundColor = Core.Navegacion,
                FontAttributes = FontAttributes.Bold,
                TextColor = Core.Titulos,
                FontSize = 25,
                CornerRadius = 20
            };

            Resultado = new Label

            {
                Text = "Resultado",
                FontSize = 18,
                TextColor = Core.Textos,
                HorizontalTextAlignment = TextAlignment.Center


            };
            Notafinal = new Label

            {
                Text = estudianteglobal.NotaPromedio.ToString(),
                FontSize = 30,
                FontAttributes = FontAttributes.Bold, 
                TextColor = Core.Navegacion,
                HorizontalTextAlignment = TextAlignment.Center


            };



        }

        void AgregarVistas()
        {

            Iconoatras.GestureRecognizers.Add(Tap_GestoAtras);

            VistaPrincipal.Children.Add(BarraNavegacion,
              Constraint.RelativeToParent((p) => { return 0; }), // X
              Constraint.RelativeToParent((p) => { return 0; }), // Y
               Constraint.RelativeToParent((p) => { return p.Width; }), // Ancho
              Constraint.RelativeToParent((p) => { return p.Height * 0.083; })  //Alto
              );


            VistaPrincipal.Children.Add(TituloPagina,
            Constraint.RelativeToParent((p) => { return 0; }), // X
            Constraint.RelativeToParent((p) => { return p.Height * 0.022; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width; })  //Alto
            );


            //VistaPrincipal.Children.Add(Iconosalir,
            // Constraint.RelativeToParent((p) => { return p.Width * 0.850; }), // X
            // Constraint.RelativeToParent((p) => { return 0; }), // Y
            //  Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), // Ancho
            // Constraint.RelativeToParent((p) => { return p.Height * 0.083; })  //Alto
            // );

            VistaPrincipal.Children.Add(Iconoatras,
             Constraint.RelativeToParent((p) => { return 0; }), // X
             Constraint.RelativeToParent((p) => { return 0; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height * 0.083; })  //Alto
             );
            VistaPrincipal.Children.Add(titulonota,
             Constraint.RelativeToParent((p) => { return p.Width * 0.10; }), // X
             Constraint.RelativeToParent((p) => { return p.Height * 0.120; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height; })  //Alto
             );

            VistaPrincipal.Children.Add(Nota1,
             Constraint.RelativeToParent((p) => { return p.Width * 0.10; }), // X
             Constraint.RelativeToParent((p) => { return p.Height * 0.161; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height * 0.173; })  //Alto
             );
            VistaPrincipal.Children.Add(titulonota2,
             Constraint.RelativeToParent((p) => { return p.Width * 0.61; }), // X
             Constraint.RelativeToParent((p) => { return p.Height * 0.120; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height ; })  //Alto
             );
            VistaPrincipal.Children.Add(Nota2,
             Constraint.RelativeToParent((p) => { return p.Width * 0.61; }), // X
             Constraint.RelativeToParent((p) => { return p.Height * 0.161; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height * 0.173; })  //Alto
             );
            VistaPrincipal.Children.Add(titulonota3,
             Constraint.RelativeToParent((p) => { return p.Width * 0.34; }), // X
             Constraint.RelativeToParent((p) => { return p.Height * 0.38; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height ; })  //Alto
             );
            VistaPrincipal.Children.Add(Nota3,
             Constraint.RelativeToParent((p) => { return p.Width * 0.34; }), // X
             Constraint.RelativeToParent((p) => { return p.Height * 0.42; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width * 0.309; }), // Ancho
             Constraint.RelativeToParent((p) => { return p.Height * 0.173; })  //Alto
             );

            VistaPrincipal.Children.Add(Resultado,
            Constraint.RelativeToParent((p) => { return p.Width * 0.28; }), // X
            Constraint.RelativeToParent((p) => { return p.Height * 0.79; }), // Y
            Constraint.RelativeToParent((p) => { return p.Width * 0.45; }), // Ancho
            Constraint.RelativeToParent((p) => { return p.Height * 0.029; })  //Alto
            );

            VistaPrincipal.Children.Add(Notafinal,
            Constraint.RelativeToParent((p) => { return p.Width * 0.28; }), // X
            Constraint.RelativeToParent((p) => { return p.Height * 0.85; }), // Y
            Constraint.RelativeToParent((p) => { return p.Width * 0.45; }) // Ancho
            //Constraint.RelativeToParent((p) => { return p.Height * 0.029; })  //Alto
            );


            Content = VistaPrincipal;

        }

        void AgregarEventos()
        {

            Tap_GestoAtras.Tapped += Tap_GestoAtras_Tapped;

        }


        private async void Tap_GestoAtras_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaginaPrincipal());
            Navigation.RemovePage(this);

        }

    }
}