﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obje2020
{
    public class VistaMenuMaster : ContentPage
    {
        public VistaMenuMaster()
        {
            Title = "Mi Vista";
            RelativeLayout Vista = new RelativeLayout { BackgroundColor = Core.Titulos };
            RelativeLayout VistaLista = new RelativeLayout { BackgroundColor = Core.Navegacion };


            List<MasterMenu> Menu = new List<MasterMenu>
            {
                new MasterMenu {Titulo = "Mis materias y notas", Icono ="intro.png", IconoVisible=true },
                new MasterMenu {Titulo = "Mi documentación", Icono ="app.png", IconoVisible=true },
                new MasterMenu {Titulo = "Cerrar sesión", Icono ="Icon.png", IconoVisible=false }

            };

            ListView listView = new ListView
            {
                ItemsSource = Menu,
                ItemTemplate = new DataTemplate(typeof(EstiloTemplate)),
                SeparatorColor = Core.Titulos,
                RowHeight = 50,
                BackgroundColor = Core.Navegacion


            };

            Button Iam = new Button {
            
            BackgroundColor = Core.Titulos,
            BorderWidth = 2,
            BorderColor = Core.Navegacion,
            Text = "Proyecto Utap",
            FontSize = 20,
            TextColor = Core.Navegacion,
            CornerRadius = 20



            };


            listView.ItemSelected += ListView_ItemSelected;



            listView.ItemTapped += ListView_ItemTapped;

          Vista.Children.Add(VistaLista,
          Constraint.RelativeToParent((p) => { return 0; }), // X
          Constraint.RelativeToParent((p) => { return p.Height * 0.167; }), // Y
           Constraint.RelativeToParent((p) => { return p.Width; }), // Ancho
          Constraint.RelativeToParent((p) => { return p.Height; })  //Alto

          );

            Vista.Children.Add(listView,
         Constraint.RelativeToParent((p) => { return 0; }), // X
         Constraint.RelativeToParent((p) => { return p.Height * 0.240; }), // Y
          Constraint.RelativeToParent((p) => { return p.Width; }), // Ancho
         Constraint.RelativeToParent((p) => { return p.Height; })  //Alto

         );

            Vista.Children.Add(Iam,
          Constraint.RelativeToParent((p) => { return p.Width * 0.105; }), // X
          Constraint.RelativeToParent((p) => { return p.Height * 0.115; }), // Y
           Constraint.RelativeToParent((p) => { return p.Width * 0.80; }), // Ancho
          Constraint.RelativeToParent((p) => { return p.Height * 0.118; })  //Alto

          );

            Content = Vista;




        }

        private async void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            switch (((MasterMenu)e.Item).Titulo)
            {
                case "Mis materias y notas":

                        break;
               
                case "Mi documentación":

                    break;
                case "Cerrar sesión":

                    await App.Current.MainPage.Navigation.PushAsync(new LoginPrincipal());
                    Page Pagina = App.Current.MainPage.Navigation.NavigationStack[0];
                    Navigation.RemovePage(Pagina);



                    break;

                default:

                    break;


            }
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }
    }
}