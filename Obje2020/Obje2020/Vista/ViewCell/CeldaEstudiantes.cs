﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obje2020
{
    public class CeldaEstudiantes : ViewCell
        
    {
        StackLayout VistaGeneral, VistaLabel;
        Label LNombre, LDocente, LNota,LMateria;


        public CeldaEstudiantes()
        {

            VistaGeneral = new StackLayout {
            
            Orientation = StackOrientation.Horizontal,
            Margin = new Thickness(12,0,12,0)
            
            
            };

            VistaLabel = new StackLayout { 
            
            HorizontalOptions = LayoutOptions.StartAndExpand
            
            
            };

            LNombre = new Label { 
            
            };



            LMateria = new Label
            {


            };


            LDocente = new Label {
            
            
            };

            LNota = new Label {
            
                FontSize = 30,
                TextColor = Core.Navegacion,
                VerticalOptions = LayoutOptions.Center


            };




            LNombre.SetBinding(Label.TextProperty, "NombreCompleto");
            LMateria.SetBinding(Label.TextProperty, "Materia");
            LDocente.SetBinding(Label.TextProperty, "Docente");
            LNota.SetBinding(Label.TextProperty, "NotaPromedio");

            VistaLabel.Children.Add(LNombre);
            VistaLabel.Children.Add(LDocente);
            VistaLabel.Children.Add(LMateria);

            VistaGeneral.Children.Add(VistaLabel);
            VistaGeneral.Children.Add(LNota);

            View = VistaGeneral;



        }
    }
}