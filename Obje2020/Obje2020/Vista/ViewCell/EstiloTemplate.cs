﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obje2020
{
    public class EstiloTemplate : ViewCell
    {
        public EstiloTemplate()
        {
            StackLayout CeldaPadre = new StackLayout
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Padding = 10
                



            };
            StackLayout Contenedor = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand
                


            };

            Image image = new Image
            {
                WidthRequest = 40,
                HeightRequest = 40

            };

            Label label = new Label
            {
            VerticalTextAlignment = TextAlignment.Center,
            TextColor = Core.Titulos
            
            
            };

            image.SetBinding(Image.SourceProperty, "Icono");
            image.SetBinding(Image.IsVisibleProperty, "IconoVisible");
            label.SetBinding(Label.TextProperty, "Titulo");


            Contenedor.Children.Add(image);
            Contenedor.Children.Add(label);
            CeldaPadre.Children.Add(Contenedor);

            View = CeldaPadre;

        }
    }
}