﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Obje2020
{
    public class LoginPrincipal : ContentPage
    {

        RelativeLayout Principal;
        Entry usuarioCaja;
        Entry passwordCaja, DocumentoCaja;
        Button botonIniciar;
        Label Recordar, Bienvenidos;
        Image Institucional;
        Cargando loading;

        TapGestureRecognizer Tap_Recordar;

        //bool respuesta;


        //CheckBox checkRecordarme;

        public LoginPrincipal()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();

        }

        void CrearVistas()
        {
            Tap_Recordar = new TapGestureRecognizer();

            Institucional = new Image 
            
            
            {
            
            Source = Core.Institucional
            
            };

            loading = new Cargando();

            DocumentoCaja = new Entry
            {

                Text = "",
                Placeholder = "No. Documento",
                PlaceholderColor = Core.Textos,
                FontSize = 13
               
                


            };

            usuarioCaja = new Entry
            {

                Text = "",
                Placeholder = "Código estudiante",
                PlaceholderColor = Core.Textos,
                FontSize = 13


            };
            passwordCaja = new Entry
            {
                Text = "",
                Placeholder = "Contraseña",
                PlaceholderColor = Core.Textos,
                FontSize = 13,

                IsPassword = true
            };

            //checkRecordarme = new CheckBox
            //{

            //    Color = Core.Botones,
            //    Margin = 0

            //};


            Recordar = new Label
            {
                Text = "¿Olvidaste tu contraseña?",
                FontSize = 13


            };

            Bienvenidos = new Label
            {
                Text = "Bienvenidos",
                FontSize = 30,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Core.Titulos
                



            };


            botonIniciar = new Button
            {
                Text = "Acceder a la App",
                BackgroundColor = Core.Botones,
                TextColor = Core.Navegacion,
                BorderColor = Core.Textos,
                CornerRadius = 20
            };



            Principal = new RelativeLayout { BackgroundColor = Core.Fondos };

            


        }
        void AgregarVistas()
        {
            Recordar.GestureRecognizers.Add(Tap_Recordar);

            Principal.Children.Add(Institucional,
            Constraint.RelativeToParent((p) => { return p.Width * 0.168; }),
             Constraint.RelativeToParent((p) => { return 86; }),
             Constraint.RelativeToParent((p) => { return p.Width * 0.706; }) // Ancho

             );

            Principal.Children.Add(Bienvenidos,
             Constraint.RelativeToParent((p) => { return p.Width * 0.098; }),
              Constraint.RelativeToParent((p) => { return 232; }),
              Constraint.RelativeToParent((p) => { return p.Width * 0.80; }) // Ancho

              );

            Principal.Children.Add(DocumentoCaja,
              Constraint.RelativeToParent((p) => { return 64; }),
               Constraint.RelativeToParent((p) => { return 332; }),
               Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), // Ancho
               Constraint.RelativeToParent((p) => { return p.Height * 0.050; }) //Alto

               );

            Principal.Children.Add(usuarioCaja,
              Constraint.RelativeToParent((p) => { return 64; }),
               Constraint.RelativeToParent((p) => { return 379; }),
              Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), // Ancho
               Constraint.RelativeToParent((p) => { return p.Height * 0.050; }) //Alto

               );

             Principal.Children.Add(passwordCaja,
              Constraint.RelativeToParent((p) => { return 64; }),
               Constraint.RelativeToParent((p) => { return 423; }),
               Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), // Ancho
               Constraint.RelativeToParent((p) => { return p.Height * 0.050; }) //Alto
               );
        
           

            //Principal.Children.Add(checkRecordarme,
            // Constraint.RelativeToParent((p) => { return 45; }),
            // Constraint.RelativeToParent((p) => { return 442; })
             
            //   );


            Principal.Children.Add(botonIniciar,
            Constraint.RelativeToParent((p) => { return 64; }),
            Constraint.RelativeToParent((p) => { return 533; }),
            Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), // Ancho
            Constraint.RelativeToParent((p) => { return p.Height * 0.065; })//Alto

              );
            Principal.Children.Add(Recordar,
            Constraint.RelativeToParent((p) => { return 110; }),
            Constraint.RelativeToParent((p) => { return 602; }),
            Constraint.RelativeToParent((p) => { return p.Width * 0.60; }), // Ancho
            Constraint.RelativeToParent((p) => { return p.Height * 0.03; })//Alto

              );

            Principal.Children.Add(loading,
           Constraint.RelativeToParent((p) => { return 0; }), // X
           Constraint.RelativeToParent((p) => { return 0; }), // Y
            Constraint.RelativeToParent((p) => { return p.Width; }), // Ancho
           Constraint.RelativeToParent((p) => { return p.Height; })  //Alto
           );


            Content = Principal;

        }

        void AgregarEventos()
        {

            //checkRecordarme.CheckedChanged += checkRecordarme_CheckedChanged;
            botonIniciar.Clicked += BotonIniciar_Clicked;

            Tap_Recordar.Tapped += Tap_Recordar_Tapped;


        }

        private async void Tap_Recordar_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RecuperarCuenta());
        }

        private async void BotonIniciar_Clicked(object sender, EventArgs e)
        {

            loading.IsVisible = true;
            await Task.Delay(1000);
            loading.IsVisible = false;

            if (string.IsNullOrEmpty(DocumentoCaja.Text))
            {


                await DisplayAlert("¡Aviso!", "Ingresa tu documento de identidad", "Aceptar");

            }
            else if (!DocumentoCaja.Text.ToCharArray().All(Char.IsDigit)) {


                await DisplayAlert("¡Aviso!", "El campo documento acepta sólo digitos numéricos", "Aceptar");


            }
            else if (!usuarioCaja.Text.ToCharArray().All(Char.IsDigit))
            {


                await DisplayAlert("¡Aviso!", "El campo Código acepta sólo digitos numéricos", "Aceptar");


            }


            else if (string.IsNullOrEmpty(usuarioCaja.Text))
            {


                await DisplayAlert("¡Aviso!", "Ingresa tu código", "Aceptar");

            }
           
            else if (string.IsNullOrEmpty(passwordCaja.Text))
            {


                await DisplayAlert("¡Aviso!", "Ingresa tu password", "Aceptar");


            }

            //else if (!string.IsNullOrEmpty(passwordCaja.Text))
            //{

            //    int count = 0;

            //    for (int i = 0; i < (passwordCaja.Text).Length; i++)
            //    {
            //        count++;
            //    }

            //    if (count <= 6)
            //    {
            //        await DisplayAlert("¡Aviso!", "Tu password debe contener 8 o más caracteres", "Aceptar");
            //    }

            //}

            else if (!string.IsNullOrEmpty(DocumentoCaja.Text))
            {

                int contador = 0;

                for (int i = 0; i < (DocumentoCaja.Text).Length; i++)
                {
                    contador++;
                }

                if (contador <= 6)
                {
                    await DisplayAlert("¡Aviso!", "Tu documento debe contener minimo 8 caracteres", "Aceptar");


                }
                else {

                    await Navigation.PushAsync(new MasterPage());
                    Navigation.RemovePage(this);

                }
            }
            else
                {

                    //if (respuesta)
                    //{
                    //    await DisplayAlert("Recuerda", "Estás guardando tu sesión", "Aceptar");

                    //    await Navigation.PushAsync(new PaginaPrincipal());
                    //}
                    //else
                    //{
                    await Navigation.PushAsync(new MasterPage());
                    Navigation.RemovePage(this);
                    //}
                }
            }







        }
        //private async void checkRecordarme_CheckedChanged(object sender, CheckedChangedEventArgs e)
        //{
        //    respuesta = await DisplayAlert("Notificacion", "¿Quieres guardar tu sesión?", "Aceptar", "Cancelar");




        //}



    }
