﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obje2020
{
    public class Cargando : RelativeLayout
    {
        ActivityIndicator Loading;

        public Cargando()
        {
            IsVisible = false;
            BackgroundColor = Core.Navegacion.MultiplyAlpha(0.6);
            Loading = new ActivityIndicator
            {
                Color = Core.Titulos,
                IsRunning = true,
                IsVisible = true

            };

            Children.Add(Loading,
           Constraint.RelativeToParent((p) => { return p.Width * 0.488; }), // X
           Constraint.RelativeToParent((p) => { return p.Height * 0.484; }), // Y
            Constraint.RelativeToParent((p) => { return p.Width * 0.106; }), // Ancho
           Constraint.RelativeToParent((p) => { return p.Height * 0.059; })  //Alto
           );


        }
    }
}