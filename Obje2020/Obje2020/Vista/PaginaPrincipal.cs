﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Obje2020
{
    public class PaginaPrincipal : ContentPage
    {

        RelativeLayout VistaPrincipal;
        BoxView BarraNavegacion;
        Image Iconosalir;
        //Image Iconoatras;
        Label TituloPagina;

        TapGestureRecognizer Tap_GestoAtras;
        List <Estudiante> ListaEstudiantes ;
        ListView ListviewEstudiantes;
        Cargando loading;



        public PaginaPrincipal()
        {
            //NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();

        }

        void CrearVistas( )
        {
            Title = "Pagina Principal";


            loading = new Cargando();

            VistaPrincipal = new RelativeLayout();

            Tap_GestoAtras = new TapGestureRecognizer();


            ListaEstudiantes = new List<Estudiante>();
            LlenadodeEstudiantesQuemados();



            BarraNavegacion = new BoxView
            {

                BackgroundColor = Core.Navegacion
            
            
            };

            Iconosalir = new Image

            {

                Source = Core.IconoSalir

            };

            //Iconoatras = new Image

            //{

            //    Source = Core.IconoAtras
                

            //};

            TituloPagina = new Label 
            
            {
            Text = "Pagina Principal",
            FontSize = 21,
            TextColor = Core.Titulos,
            HorizontalTextAlignment = TextAlignment.Center
            
            
            };



            ListviewEstudiantes = new ListView 

            { 
              ItemsSource = ListaEstudiantes,
              ItemTemplate = new DataTemplate(typeof(CeldaEstudiantes)),
              RowHeight = 80

            };

             

            //Principal = new RelativeLayout { BackgroundColor = Color.White };




            //nombreusuario = new Label
            // {
            //   Text = ("Mi nombre es: " + App.sesion.Nombre + " " + App.sesion.Apellido + " y mi correo es:  " + Myuserlast.Correo + "   ¿Cuál es mi edad?.Tap aquí"),
            // TextColor = Color.DarkBlue,
            //FontSize = 20


            //  };

            //taplabel = new TapGestureRecognizer();

        }
        void AgregarVistas()
        {

            Iconosalir.GestureRecognizers.Add(Tap_GestoAtras);

            //VistaPrincipal.Children.Add(BarraNavegacion,
            //  Constraint.RelativeToParent((p) => { return 0; }), // X
            //  Constraint.RelativeToParent((p) => { return 0 ; }), // Y
            //   Constraint.RelativeToParent((p) => { return p.Width ; }), // Ancho
            //  Constraint.RelativeToParent((p) => { return p.Height * 0.083; })  //Alto
            //  );


            //VistaPrincipal.Children.Add(TituloPagina,
            //Constraint.RelativeToParent((p) => { return 0; }), // X
            //Constraint.RelativeToParent((p) => { return p.Height * 0.022; }), // Y
            // Constraint.RelativeToParent((p) => { return p.Width; })  //Alto
            //);


            //VistaPrincipal.Children.Add(Iconosalir,
            // Constraint.RelativeToParent((p) => { return p.Width * 0.850 ; }), // X
            // Constraint.RelativeToParent((p) => { return 0; }), // Y
            //  Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), // Ancho
            // Constraint.RelativeToParent((p) => { return p.Height * 0.083; })  //Alto
            // );

            //VistaPrincipal.Children.Add(Iconoatras,
            // Constraint.RelativeToParent((p) => { return 0; }), // X
            // Constraint.RelativeToParent((p) => { return 0; }), // Y
            //  Constraint.RelativeToParent((p) => { return p.Width * 0.149; }), // Ancho
            // Constraint.RelativeToParent((p) => { return p.Height * 0.083; })  //Alto
            // );

            VistaPrincipal.Children.Add(ListviewEstudiantes,
            Constraint.RelativeToParent((p) => { return 0; }), // X
            Constraint.RelativeToParent((p) => { return 0 ; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width ; }), // Ancho
            Constraint.RelativeToParent((p) => { return p.Height * 0.824; })  //Alto
            );

            VistaPrincipal.Children.Add(loading,
            Constraint.RelativeToParent((p) => { return 0; }), // X
            Constraint.RelativeToParent((p) => { return 0; }), // Y
             Constraint.RelativeToParent((p) => { return p.Width; }), // Ancho
            Constraint.RelativeToParent((p) => { return p.Height; })  //Alto
            );


            Content = VistaPrincipal;

        }

        void AgregarEventos()
        {

            Tap_GestoAtras.Tapped += Tap_GestoAtras_Tapped;

           
            
            ListviewEstudiantes.ItemSelected += ListViewEstudiantes_ItemSelected;
            ListviewEstudiantes.ItemTapped += ListviewEstudiantes_ItemTapped;


          //  nombreusuario.GestureRecognizers.Add(taplabel);
           // taplabel.Tapped += Taplabel_Tapped;
        }

        private async void ListviewEstudiantes_ItemTapped(object sender, ItemTappedEventArgs e)
        {

                loading.IsVisible = true;
                await Task.Delay(1000);
                loading.IsVisible = false;

            await Navigation.PushAsync(new DetalleNotas((Estudiante)e.Item)) ;
            Navigation.RemovePage(this);

        }

        private async void Tap_GestoAtras_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();

        }



        private void ListViewEstudiantes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }


          void LlenadodeEstudiantesQuemados() 
        {
            

            ListaEstudiantes.Add(new Estudiante

            {
                Nombre = "Ronald",
                Apellido = "Echeverry",
                Docente = "Docente  : Roney Rodriguez",
                Materia = "Electiva II",
                Nota = 4.2,
                Nota2 = 4.5,
                Nota3 = 4.2



            });


            ListaEstudiantes.Add(new Estudiante

            {
                Nombre = "Stiven",
                Apellido = "Aguirre",
                Docente = "Docente  : David Santafe",
                Materia = "Desarrollo web II",
                Nota = 4.4,
                Nota2 = 4.2,
                Nota3 = 3.8


            });

            ListaEstudiantes.Add(new Estudiante

            {

                Nombre = "Roberto",
                Apellido = "Gonzales",
                Docente = "Docente  : Carlos Rodriguez",
                Materia = "Base de Datos II",
                Nota = 5.0,
                Nota2 = 4.3,
                Nota3 = 3.9

            });

           
           



        }



        private async void Taplabel_Tapped(object sender, EventArgs e)
        {

            await DisplayAlert("Notificación", "Su edad es: " + App.sesion.Edad, "Aceptar");
        }
    }
}