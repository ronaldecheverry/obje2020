﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obje2020
{
    public class Estudiante
    {

        public  string Nombre { get; set; }
        public  string Apellido { get; set; }
        public  string Materia { get; set; }
        public  string Docente { get; set; }
        public  double Nota { get ; set; }
        public double Nota2 { get; set; }
        public double Nota3 { get; set; }
        public  string NombreCompleto { get { return Nombre + " " + Apellido; } }

        public double NotaPromedio { get { return ((Nota * 30)/100 + (Nota2* 30)/100 + (Nota3 * 40/100)) ; } }

    }

}
