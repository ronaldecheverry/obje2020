﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Obje2020
{
    public static class Core
    {

        //Seccion Colores

        public static Color Navegacion { get; } = Color.FromHex("#54ADFF");
        public static Color Botones { get; } = Color.FromHex("#FFFFFF");
        public static Color Textos { get; } = Color.FromHex("#313131");
        public static Color Titulos { get; } = Color.FromHex("#FFFFFF");

        public static Color Fondos { get; } = Color.FromHex("#54ADFF");


        // Seccion Images Iconos

        public static ImageSource Institucional { get; } = ImageSource.FromFile("Institucional.png");

        public static ImageSource IconoSalir { get; } = ImageSource.FromFile("IconoSalir.png");

        public static ImageSource IconoAtras { get; } = ImageSource.FromFile("IconoAtras.png");
    }
}